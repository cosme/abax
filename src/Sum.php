<?php

require_once('MeasuresArray.php');

class Sum {
    
    private $sum;
    private $error;
    private $template;
    private $result;
    private $ratioArray;
    private $measuresArray;

    
    public function __construct( )
    {
        $this->sum = 0;
        $this->error = null;
        $this->template = "";
        $this->result = new StdClass();
        $this->measuresArray = new StdClass();
    }
    
    /* set system */
    public function setMeasuresSystem( $measures ) {

        $this->measuresArray = $measures->measuresArray;        
        
        $this->result->resultString = '';
        
        foreach( $this->measuresArray as $num => $measure ) {
           $name = $measure->name;      
           $this->result->$num = 0;
           $this->template .= '%'.$measure->name.'% '.$measure->name.'. ';
        }
        
        $this->setLowestRatio();

        return $this;
    }
    
    /*
    * add a single value to the sum
    */
    public function addSingleValue ( $name, $value) {
        
        if ($this->validValue($value) === false) {
            return $this;
        }
        
        $measure = $this->getMeasure ( $name );
        
        if ( $measure != null ) {
            $this->sum += $this->createSum($measure, $value);
            $this->convertSum();
        } else {
            $this->error = "property does not exist";
        }

        return $this;
    }
    
    /*
    * remove a single value from the sum
    */
    public function removeSingleValue ( $name, $value) {
        
        if ($this->validValue($value) === false) {
            return $this;
        }
        
        $measure = $this->getMeasure ( $name );

        if ( $measure != null ) {
            $tempSum = $this->sum - $this->createSum($measure, $value);
            if($tempSum < 0) {
                $this->error = 'La somme est inférieure à 0';
            } else {
                $this->sum = $tempSum;
                return $this->convertSum();
            }
        } else {
            $this->error = "property does not exist";
        }

        return $this;
    }
    
    /*
    * getSingleValue
    */
    public function getSingleValue( $name ) {
        
        $measure = $this->getMeasure ( $name );
        if( $measure != null ) {
            return $measure->name;
        }
        
        return null;
    }
    
    /*
    * add an array of values to the sum
    */
    public function addValues ( &$values) {

        foreach($values as $name => $value) {
            $this->addSingleValue($name, $value);
            if($this->error != null) {
                break;
            }
        }
        
        return $this;
    }
    
    /*
    * remove an array of values from the sum
    */
    public function removeValues ( &$values) {

        foreach($values as $name => $value) {
            $this->removeSingleValue($name, $value);
            if($this->error != null) {
                break;
            }
        }
        
        return $this;
    }
    
    /*
    * getLowestRatio
    */
    public function setLowestRatio( ) {
        $ratios = [];
        foreach($this->measuresArray as $name => $measure) {
            $ratios[$name] = $measure->ratio;
        }     
        end($ratios);
        
        $this->lowestRatio = key($ratios);
        
        return $this;
    }
        
    /* 
    * Convert all values to lowest ratio
    */
    public function createSum ( $measure, $value ) {
        
        $tempSum = 0;
        $measures = $this->measuresArray;
        $lowestRatioName = $this->lowestRatio;
        
        $ratioMath = $this->getRatio( $measure->ratio );
        $lowestRatioMath = $this->getRatio( $measures->$lowestRatioName->ratio );
        $result = $value * ( $ratioMath / $lowestRatioMath );

        $tempSum += $result;
        return $tempSum;

    }
    
    /* 
    * Convert to values from lowest ratio sum
    */ 
    public function convertSum () {

        $sum = $this->sum;
        $measures = $this->measuresArray;
        $lowestRatioName = $this->lowestRatio;
        
        foreach($measures as $num => $measure) {
            $ratioMath = $this->getRatio( $measure->ratio );
            $lowestRatioMath = $this->getRatio( $measures->$lowestRatioName->ratio );

            $ratio = $ratioMath / $lowestRatioMath;

            if ( bccomp($ratioMath, $lowestRatioMath, 10) == 0) {
                $result = bcdiv($sum, $ratio, 5);
                $temp = $result * $ratio;
            } else {
                $result = bcdiv($sum, $ratio);
                $temp = $result * $ratio;
            }
            
            $result = preg_replace('/0{2,}$/', '', $result);
            $result = preg_replace('/\.$/', '', $result);
            $this->result->$num = $result;
            
            $sum = bcsub($sum,$temp,5);
        }
        
        return $sum;

    }
    
    
    /* 
    * getRatio
    */ 
    public function getRatio ( $ratio ) {    
        if( preg_match('/[0-9]+\/[0-9]+/', $ratio) ) {
            $mathTemp = explode('/', $ratio);    
            $math = (int)$mathTemp[0] / (int)$mathTemp[1];
        } else {
            $math = (int)$ratio;
        }
        return $math;
    }
    
    public function getMeasure ( $name ) {    
               
        foreach($this->measuresArray as $measure) {
            if( $measure->name == $name ) {
                return $measure;
            }
        }
    
        return null;
    }


    /*
    * getResultString
    */
    public function getResultString() {

        $template = $this->template;
        $measures = $this->measuresArray;

        foreach($measures as $num => $measure) {
            $template = str_replace('%'.$measure->name.'%', $this->result->$num, $template);
        }
        
        $this->result->resultString = $template;

        return $this->result->resultString;
    }
    
    /*
    * is the value valid
    */
    public function validValue($value) {
        
        $value = str_replace(',', '.', $value);
        $this->error  = null;
        
        if( $value != '' && !preg_match('/^([0-9]+|[0-9]+\.[0-9]+)$/', $value) ) {
            $this->error = "La valeur n'est pas correcte";
            return false;
        }
        
        return true;
    }
    
    /*
    * getError
    */
    public function getError() {
        return $this->error;
    }
    
}

?>