<?php

class Tools {
    
    
    function convert($string, $return = 0){
        
        if($string === 0 || $string === '0') {
            return $string;
        }

        $returnTemp = 0;

        preg_match('/([a-z]+)/', $string, $matches);

        if(isset ($matches[1]) and $matches[1] != null ){

                $tempString = explode($matches[1], $string);
                $returnTemp += self::romainVersArabe( $tempString[0] );
                $multiple = self::romainVersArabe( strtoupper($matches[1]) );
                $returnTemp = $returnTemp * $multiple;
                $return += $returnTemp;
                $string = str_replace($tempString[0].$matches[1], '', $string);
                $returnTemp = self::convert( $string, $return);

        } else {
            
            $returnTemp = $return + self::romainVersArabe( $string );
            
        }
        
        $return = $returnTemp;
        
        return $return;
        
        
        
    }

    function romainVersArabe( $roman ) {

        if($roman === 0 || $roman === '0') {
            return $roman;
        }
        
        $numbers = array(
            'I' => 1,
            'V' => 5,
            'X' => 10,
            'L' => 50,
            'C' => 100,
            'D' => 500,
            'M' => 1000,
        );

        $romanTemp = str_replace(' ', '', $roman);
        $roman = $romanTemp;
        $length = strlen( $roman );

        $counter = 0;
        $dec = 0;
        
        while ( $counter < $length ) {
            if ( ( $counter + 1 < $length ) && ( $numbers[$roman[$counter]] < $numbers[$roman[$counter + 1]] ) ) {
                $dec += $numbers[$roman[$counter + 1]] - $numbers[$roman[$counter]];
                $counter += 2;
            } else {
                $dec += $numbers[$roman[$counter]];
                $counter++;
            }
        }


        return $dec;
        
    }
    
    
    function clean( $string ) {

        $remove = array('lb.', 'lb', 'libras', 's.', 'd.', '.', ',', 'denarios', 'denarius', 'obolos', 'obolum');
        $return = str_replace($remove, '', $string);
        
        if($return == '') {
            $return = 'I';
        }
        
        return $return;
        
    }
    
}
















?>