<?php


class MeasuresArray {
    
    public $measuresArray;

    public function __construct( &$array = null )
    {
        $this->measuresArray = new StdClass();
        
        if( $array == null ) {
            $array = array(
                0 => array ('name' => 'base', 'abbr' => 'base', 'ratio' => '1')
            );
        } else {
            uasort($array, function($a, $b) {
                $ratioA = $this->getRatio($a['ratio']);
                $ratioB = $this->getRatio($b['ratio']);
                return $ratioA < $ratioB;
            });
        }

        $this->setMeasuresArray( $array );
        return $this;
    }
    
    /*
    * setMeasuresArray
    */
    public function setMeasuresArray ( $array ) {
        $this->measuresArray = new StdClass(); 
        
        foreach($array as $num => $arr){
            $this->setMeasure( $num, $arr );
        }
        
        return $this;
    }
    
    /*
    * getMeasureArray
    */
    public function getMeasureArray ( ) {
        return $this->measuresArray; 
    }
    
    
    /*
    * getBase
    */
    public function getBase ( ) {
        
        $measuresArray = $this->measuresArray;
        foreach( $measuresArray as $measure ) {
            if($measure->ratio == 1) {
                return $measure;
            }
        }
        
        return null;
    }

    /*
    * getMeasureList
    */
    public function getMeasuresNames ( ) {
        
        $return = array();
        $list = $this->measuresArray;
        foreach ($list as $name => $l) {
            $return[] = $name;
        }
    
        return $return; 
    }
    
    /*
    * setMeasure
    */
    public function setMeasure ( $num, $arr ) {
       
        $this->measuresArray->$num = new StdClass(); 
        $this->measuresArray->$num->name = $arr['name'];
        $this->measuresArray->$num->abbr = $arr['abbr'];
        $this->measuresArray->$num->ratio = $arr['ratio'];

        return $this;
    }

    /*
    * getMeasure
    */
    public function getMeasure ( $name ) {
        return $this->measuresArray->$name; 
    }
    
    /*
    * getRatio
    */
    public function getRatio ( $ratio ) {
        if( preg_match('/[0-9]+\/[0-9]+/', $ratio) ) {
            $math2 = explode('/', $ratio);    
            $ratioMath = (int)$math2[0] / (int)$math2[1];
        } else {
            $ratioMath = (int)$ratio;
        }
        return $ratioMath;
    }

}












?>