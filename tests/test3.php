<?php
session_start();

require_once('autoload.php');

$referentiels = new TEIdocument('referentials.xml');
$listes = $referentiels->getDoc()->query('//referential/@name');
foreach( $listes as $l ){
    $data['liste'][] = $l->value;
}

if(isset($_SESSION['custom'])){
    foreach ($_SESSION['custom'] as $name => $custom){
        $data['liste'][] = $name;
    }
}

$data['path'] = $_SERVER['PHP_SELF'];

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array('debug' => true));
$twig->addExtension(new Twig_Extension_Debug());

if($_GET != null && isset($_GET['type'])){
    
    if( isset($_SESSION['current_type']) && $_SESSION['current_type'] != $_GET['type'] ) {
        $_SESSION["prev_sommes"] = [];
        $data['somme'] = 0;
    }
    
    $_SESSION['current_type'] = $_GET['type'];
    
    $data['type'] = $_GET['type'];
    

    $measureArray = [];
    
    foreach($listes as $liste) {

        if( $liste->value == $_GET['type'] ) {
            $measures = $referentiels->getDoc()->query('./ancestor-or-self::referential/measure', $liste);
            
            foreach ($measures as $m){

                $measureArray[] = array(
                    'name' => $referentiels->getDoc()->query('./name', $m)->item(0)->nodeValue,
                    'abbr' => $referentiels->getDoc()->query('./abbr', $m)->item(0)->nodeValue,
                    'ratio' => $referentiels->getDoc()->query('./ratio', $m)->item(0)->nodeValue
                );
                
            }

        } elseif (isset($_SESSION['custom'])) {
            foreach ($_SESSION['custom'] as $name => $custom){
                
                if( $name == $_GET['type'] && isset($custom['measureArray']) ) {
                    $measureArray = $custom['measureArray'];
                }
                
            }    
        }
    }    
    
    
    $data['referentiel'] = $measureArray;

    $measures = new MeasuresArray($measureArray);   
    $somme = new Sum();
    $somme->setMeasuresSystem( $measures );

    
    if($_POST != null ){

        print_r($_POST);

        $type = null;
        if( isset($_POST['type']) ){
            $type = $_POST['type'];
            unset( $_POST['type'] );
        }

        if( isset($_POST['reset']) && $_POST['reset'] == 1 ) {
            $_SESSION["prev_sommes"] = [];
            $data['somme'] = 0;
        } else {
            $prevArray = [];
            if(isset($_SESSION["prev_sommes"])){
                foreach($_SESSION["prev_sommes"] as $prev){
                    $prevType = $prev['type'];
                    unset ($prev['type']);
                    if($prevType == 'ajouter') {
                        $somme->addValues( $prev );
                    } elseif($prevType == 'soustraire') {
                        $somme->removeValues( $prev );
                    }
                    $prev['type'] = $prevType;
                    $prevArray[] = $prev;
                }
            }

            if($type == 'ajouter') {
                $somme->addValues( $_POST );
            } elseif($type == 'soustraire') {
                $somme->removeValues( $_POST );
            }
                        
            $new = $_POST;
            $new['type'] = $type;

            if($somme->getError() != null) {
                $data['error'] = $somme->getError();
            } else {
                $prevArray[] = $new;
                $_SESSION["prev_sommes"] = $prevArray;
                $data['somme'] = $somme->getResultString();
            }

        }
        
    }

}

if (isset($_SESSION["prev_sommes"])){
    $data['prev_sommes'] = $_SESSION["prev_sommes"];
}

echo $twig->render('main.html.twig', $data);
  

?>