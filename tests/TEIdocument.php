<?php


class TEIdocument {
    
    private $doc;
    private $xpath;
    
    public function __construct($doc){
        
        $ref = new DomDocument();
        $ref->load($doc);
        
        $xpath = new DOMXpath($ref);

        $this->doc = $ref;
        $this->xpath = $xpath;
        
        return $this;
    }
    
    public function getDoc() {
        return $this->xpath;
    }
   
   
    
}

?>