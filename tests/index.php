<?php


error_reporting(E_ALL); 
require_once('MeasuresArray.php');
require_once('Sum.php');

?>

<form method="post" action="index.php">
    <label>Système de calcul</label><br/>
    <input type="radio" name="livres-sous-deniers" value="1/20/12" checked="checked">1 livre = 20 sous / 1 sous = 12 deniers </input><br/>
    <br/>
    <label>Sommes</label><br/>
    <input type="checkbox" name="val[1]" value="1/2/5">1 lb., 2 s., 5 d.</input><br/>
    <input type="checkbox" name="val[2]" value="4/19/11">4 lb., 19 s., 11 d.</input><br/>
    <input type="checkbox" name="val[3]" value="100/15/0">100 lb., 15 s.</input><br/>
    <input type="checkbox" name="val[4]" value="14/4/6">14 lb., 4 s., 6 d.</input><br/>
    <input type="checkbox" name="val[5]" value="0/2/3">2 s., 3 d.</input><br/>
    <input type="checkbox" name="val[6]" value="0/25/0">25 s.</input><br/>
    <input type="checkbox" name="val[7]" value="40/0/0">40 lb.</input><br/>
    
    <br/>
    <input type="submit" value="calculer"/>
</form>


<form method="post" action="index.php">

    <input type="submit" value="reset"/>
</form>

<?php


$measureArray[] = array(
  'name' => 'livre',
  'abbr' => 'lb',
  'ratio' => '1'
);
$measureArray[] = array(
  'name' => 'sous',
  'abbr' => 's',
  'ratio' => '1/20'
);
$measureArray[] = array(
  'name' => 'denier',
  'abbr' => 'd',
  'ratio' => '1/240'
);


$somme = new Sum();
$measures = new MeasuresArray($measureArray);   
$somme->setMeasuresSystem($measures);



if( isset($_POST)) {
    

    $error = null;

    if( isset($_POST['reset']) && $_POST['reset'] == 1 ) {
        $_SESSION["prev_sommes"] = [];
        $data['somme'] = 0;
    }



    if(isset($_POST['val'])){
        foreach($_POST['val'] as $post){

           $value = explode('/', $post);
           $values = array(
            'livre' => $value[0], 
            'sous' => $value[1], 
            'denier' => $value[2], 
            'type' => 'ajouter'
            );
           $somme = $somme->addValues($values);
                      
           /*if( $somme->getError() != null ) {
               echo $somme->getError();
               break;
           } */
        }
        
    }

    
    if($somme->getResultString() != null){
        
     echo 'somme : '.$somme->getResultString();

    }
    
    
    
}



?>
