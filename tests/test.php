<?php
require_once('ComputSystem.php');
require_once('Sum.php');
require_once('Tools.php');

    
$xml = new DomDocument();
$xml->load('xml.xml');

$xpath = new DOMXpath($xml);

$div =  $xpath->query('//div/div');



echo '<h1>AN_J326</h1>';



foreach( $div as $d ) {
    echo '<div>';
    
    $itemArray = [];
    $somme = new Sum();
    if(isset($_POST['syst'])){
        $syst = explode('/', $_POST['syst']);
        $cs = new ComputSystem($syst[0], $syst[1], $syst[2], $sys[3]);
    } else {
        $cs = new ComputSystem();
    }
    $somme->setComputSystem($cs);
    
    $title = $xpath->query('./descendant::head', $d)->item(0);
    echo '<h2>'.$title->nodeValue.'</h2>';

    $lists = $xpath->query('./descendant::list', $d);
    $summa = $xpath->query('./p[@type="summa"]', $d)->item(0);
    
    echo '<table>';
    echo '<tr><th style="width:400px;padding-left:15px;">texte</th><th>valeur</th></tr>';
    foreach( $lists as $list) {
        
        
        
        $items = $xpath->query('./descendant::item', $list);
        
        foreach( $items as $item) {
            
            $measureList = $xpath->query('./descendant::measure', $item);
            
            foreach($measureList as $measure) {
    
                $lb = 0;
                $s = 0;
                $d = 0;
                $ob = 0;
                
                $lbNode = $xpath->query('./self::*[@unit="lb"]', $measure);
                if ( $lbNode->item(0) != null ) {
                    $lb  = $lbNode->item(0)->nodeValue;
                }
                $sNode = $xpath->query('./self::*[@unit="s"]', $measure);
                if ( $sNode->item(0) != null ) {
                    $s  = $sNode->item(0)->nodeValue;
                }
                $dNode = $xpath->query('./self::*[@unit="d"]', $measure);
                if ( $dNode->item(0) != null ) {
                    $d  = $dNode->item(0)->nodeValue;
                }
                $obNode = $xpath->query('./self::*[@unit="ob"]', $measure);
                if ( $obNode->item(0) != null ) {
                    $ob  = $obNode->item(0)->nodeValue;
                }

                $lb = Tools::convert( Tools::clean($lb) );
                $s = Tools::convert( Tools::clean($s) );
                $d = Tools::convert( Tools::clean($d) );
                $ob = Tools::convert( Tools::clean($ob) );
                
                $itemArray[] = $lb.'/'.$s.'/'.$d.'/'.$ob;

            }
            
            
            
            echo '<tr><td>'.$item->nodeValue.'</td><td>'.$lb.' lb., '.$s.' s., '.$d.' d., '.$ob.' ob.</td></tr>';
        }

    }
    
    foreach($itemArray as $item) {
        $value = explode('/', $item);
        $somme = $somme->addSum($value[0],$value[1],$value[2],$value[3]);
    }
    
    // $somme = $somme->addSum(0,0,0,2);
    // print_r($somme);

    echo '<tr style="font-weight:bold;padding-top:15px;"><td >'.$summa->nodeValue.'</td><td>'.$somme->getResult().'</td></tr>';
    
    echo '</table>';
    
    echo '</div>';
    
}








?>