<?php
session_start();

require_once('autoload.php');

$data = [];

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array('debug' => true));
$twig->addExtension(new Twig_Extension_Debug());

 if($_POST != null ){

    $custom = [];
    if(isset($_POST['nom'])){
        if($_POST['nom'] != ''){
            $referentiel = $_POST['nom'];
        } else {
            $referentiel = 'custom-'.rand(1-100);
        }
        unset($_POST['nom']);
        
        foreach ($_POST as $value){

            if($value['name'] != null && $value['ratio'] != null) {
                
                if(preg_match('/^([0-9]+)|([0-9]+\/[0-9]+)$/', $value['ratio'])){
                    $custom['measureArray'][] = array(
                        'name' => $value['name'],
                        'abbr' => $value['abbr'],
                        'ratio' => $value['ratio'],
                    );
                }
            }  
        }
        
        if($custom != '') {
            $_SESSION['custom'][$referentiel] = $custom;
        }
    }

    
    if(isset($_POST['reset']) &&  $_POST['reset'] == 1){
        $_SESSION['custom'] = [];
    }
    
}

if(isset($_SESSION['custom'])){
    foreach ($_SESSION['custom'] as $name => $custom){
        $data['liste'][] = $name;
    }
}

header('Content-Type: text/html; charset=utf-8');
echo $twig->render('custom-referential.html.twig', $data);

?>