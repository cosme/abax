<?php

require_once('MeasuresArray.php');
require_once('Sum.php');
require_once('Tools.php');
require_once('Referentiels.php');

// $referentiel = array(
    // 'libra' => array ('abbr' => 'lb', 'ratio' => '1'),
    // 'solidus' => array('abbr' => 's', 'ratio' => '1/20'),
    // 'denarius' => array('abbr' => 'd', 'ratio' => '1/240'),
    // 'obolum' => array('abbr' => 'ob', 'ratio' => '1/480')
// );


// $referentiel = array(
    // 'euros' => array('abbr' => 'e', 'ratio' => '1'),
    // 'centimes' => array('abbr' => 'cts', 'ratio' => '1/100'),
    // 'briques' => array ('abbr' => 'brk', 'ratio' => '1000'),
// );

// $referentiel = array(
    // array ('name' => 'inch', 'abbr' => 'in', 'ratio' => '1/12'),
    // array('name' => 'link', 'abbr' => 'lk', 'ratio' => '66/100'),
    // array('name' => 'foot', 'abbr' => 'ft', 'ratio' => '1'),
    // array('name' => 'yard', 'abbr' => 'yd', 'ratio' => '3'),
    // array('name' => 'mile', 'abbr' => 'ml', 'ratio' => '5280'),
// );

// $referentiel = array(
    // array('name' => 'heure', 'abbr' => 'h', 'ratio' => '1'),
    // array('name' => 'minute', 'abbr' => 'm', 'ratio' => '1/60'),
    // array ('name' => 'seconde', 'abbr' => 's', 'ratio' => '1/3600'),
// );

$referentiel = array(
    array('name' => 'litron', 'abbr' => 'lt', 'ratio' => '1/16'),
    array('name' => 'quart', 'abbr' => 'qt', 'ratio' => '1/4'),
    array ('name' => 'boisseau', 'abbr' => 'b', 'ratio' => '1'),
    array ('name' => 'minot', 'abbr' => 'mt', 'ratio' => '3'),
    array ('name' => 'mine', 'abbr' => 'mi', 'ratio' => '6'),
    array ('name' => 'setier', 'abbr' => 'st', 'ratio' => '12'),
    array ('name' => 'muid', 'abbr' => 'md', 'ratio' => '144'),

);

    
$xml = new DomDocument();
$xml->load('xml.xml');

$xpath = new DOMXpath($xml);

$div =  $xpath->query('//div/div');
    
$measures = new MeasuresArray($referentiel);

$somme = new Sum();
$somme->setMeasuresSystem( $measures );

// print_r($somme);

// $values = array('briques' => 1, 'euros' => '1500', 'centimes' => 150);
// $somme->addValues( $values );
// $somme->addSingleValue('libra', 1);
// $somme->addValues($values);
// $somme->addSingleValue('mile', 2.5);
// $somme->addSingleValue('foot', 1);
// $somme->addSingleValue('link', 1.6);
// $somme->addSingleValue('foot', 522);
// $somme->addSingleValue('foot', 522);
// $somme->addSingleValue('yard', 4);
// $somme->addSingleValue('yard', 4005);
// $somme->addSingleValue('inch', 7.93);
// $somme->addSingleValue('denarius', 47);
// $somme->removeValues($values);


$values = array('heure' => 2.5, 'minute' => 75.50, 'seconde' => 2.1);
$somme->addValues( $values );
// $values = array('heure' => 1.4155);
// $somme->addValues( $values );


echo '<pre>';

echo $somme->getResultString();

print_r($somme);

   

?>